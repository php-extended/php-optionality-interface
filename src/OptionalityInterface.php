<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-optionality-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Optionality;

/**
 * OptionalityInterface interface file.
 * 
 * This interface represents optionalities. An optionality is a tolerance to
 * whether a data can be reduced to a non-value (ergo an empty value, or a
 * null, or undefined value).
 * 
 * @author Anastaszor
 */
interface OptionalityInterface
{
	
	/**
	 * Gets whether this optionality allows null or undefined values.
	 * 
	 * @return boolean
	 */
	public function isNullAllowed() : bool;
	
	/**
	 * Gets whether this optionality allows empty values. Empty values should
	 * be defined on a per-data structure basis.
	 * 
	 * @return boolean
	 */
	public function isEmptyAllowed() : bool;
	
	/**
	 * Gets whether two optionalities are equals.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
	/**
	 * Gets the optionality that is obtained by merging two optionalities. The
	 * resulting optionality MUST be more tolerant than both of the argument
	 * optionalities.
	 * 
	 * @param OptionalityInterface $other
	 * @return OptionalityInterface
	 */
	public function mergeWith(OptionalityInterface $other) : OptionalityInterface;
	
}
